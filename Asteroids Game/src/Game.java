import java.applet.*;
import java.awt.*;
import java.awt.event.*;
public class Game extends Applet implements Runnable, KeyListener{
	
	Dimension dim; // stores the size of the back buffer
	Image img; // the back buffer object
	Graphics g; // used to draw on the back buffer
	int x,y, xVelocity, yVelocity; // these are all new variables
	Thread thread;
	long startTime, endTime, framePeriod; //long is just a very big integer

	public void init(){
		resize(500,500);
		x=225; // the top left x and y coordinates of the rectangle
		y=225; // containing the circle.
		xVelocity=0;
		yVelocity=0;
		addKeyListener(this);
		startTime=0;
		endTime=0;
		framePeriod=25; // 25 milliseconds is a good frame period
		dim=getSize(); //set dim equal to the size of the applet
		img=createImage(dim.width, dim.height);//create the back buffer
		g=img.getGraphics(); //retrieve Graphics object for back buffer
		thread=new Thread(this); // create the thread
		thread.start(); // start the thread running
	}

	public void paint(Graphics gfx){
		gfx.setColor(Color.black); // clear the screen with black
		gfx.fillRect(0,0,500,500);
		gfx.setColor(Color.red);
		gfx.fillOval(x,y,50,50);
	}

	public void run(){
		for(;;){//this infinite loop ends when the webpage is exited
			startTime=System.currentTimeMillis();
			x+=xVelocity; //Moves the circle according to the x and y
			y+=yVelocity; //velocities, which are set by event handling
			repaint();
			try{
				endTime=System.currentTimeMillis();
				if(framePeriod-(endTime-startTime)>0)
					Thread.sleep(framePeriod-(endTime-startTime));
			}catch(InterruptedException e){
			}	
		}
	}

	public void keyPressed(KeyEvent e){ //starts moving the circle when an
		//arrow key is pressed
		if(e.getKeyCode()==KeyEvent.VK_W) //VK_UP is the up arrow key
			yVelocity=-1; //subtracting from y moves it upward
		else if(e.getKeyCode()==KeyEvent.VK_S)
			yVelocity=1; //adding to y moves it down
		else if(e.getKeyCode()==KeyEvent.VK_A)
			xVelocity=-1; //subtracting from x moves it left
		else if(e.getKeyCode()==KeyEvent.VK_D)
			xVelocity=1; //adding to x moves it right
	}

	public void keyReleased(KeyEvent e){ //stops moving the circle when the
		//arrow key is released
		if(e.getKeyCode()==KeyEvent.VK_W ||
			e.getKeyCode()==KeyEvent.VK_S)
			yVelocity=0; //stops vertical motion when either up or down
		//is released
		else if(e.getKeyCode()==KeyEvent.VK_A ||
			e.getKeyCode()==KeyEvent.VK_D)
			xVelocity=0; //stops horizontal motion when either right or
		//left is released
	}

	public void keyTyped(KeyEvent e){ //empty method, but still needed to
	} //implement the KeyListener interface
}

